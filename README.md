# Project_X

Project_X is a [description of your project]. It includes [brief overview of the main features or components of your project].

## Installation

To get started with Project_X, follow these steps:


1. Install all the dependencies.
    
    ```bash
    npm install --save-dev
    ```
2. Start the application.
    
    ```bash
    npx hardhat compile
    ```
3. Deploy the contract.
    
    ```bash
     npx hardhat run scripts/deploy.js --network sepolia
    ```

4. Verify the contract.
    
    ```bash
    npx hardhat verify --network sepolia [contract address]
    ```

5. Run the test.
    
    ```bash
    npx hardhat test scripts/test.js --network sepolia
    ```



