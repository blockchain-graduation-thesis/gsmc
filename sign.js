const { ethers } = require("ethers");

const privateKey =
  "5f5d27f8df70cd6f8a496d84b434567b74fc81b707040797d1c60222b74a8a71"; // Thay bằng private key của owner
const wallet = new ethers.Wallet(privateKey);

async function signMessage(user, token, amount) {
  const messageHash = ethers.utils.solidityKeccak256(
    ["address", "address", "uint256"],
    [user, token, amount]
  );
  const signature = await wallet.signMessage(
    ethers.utils.arrayify(messageHash)
  );
  console.log(`Signature: ${signature}`);
}

// Ví dụ sử dụng
signMessage("USER_ADDRESS", "TOKEN_ADDRESS", "AMOUNT");
