const { ethers } = require("hardhat");
const fs = require("fs");
require("hardhat-deploy");
require("hardhat-deploy-ethers");
const utils = ethers.utils;

async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deploying contracts with the account:", deployer.address);

  const TestERC20 = await ethers.getContractFactory("TestERC20");
  const testERC20 = await TestERC20.deploy();
  console.log("TestERC20 contract deployed to:", testERC20.address);

  const TestERC721 = await ethers.getContractFactory("TestERC721");
  const testERC721 = await TestERC721.deploy();
  console.log("TestERC721 contract deployed to:", testERC721.address);

  const DepositWithdraw = await ethers.getContractFactory("DepositWithdraw");
  const depositWithdraw = await DepositWithdraw.deploy();
  console.log("DepositWithdraw contract deployed to:", depositWithdraw.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
