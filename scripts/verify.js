async function main() {
  const { run } = require("hardhat");

  await run("verify:verify", {
    address: "0xd04E1D562B5ba501A29F72Ca15880b213C2EF0f0", // Địa chỉ của TestERC20
    contract: "contracts/TestERC20.sol:TestERC20",
  });

  await run("verify:verify", {
    address: "0xYourERC721ContractAddress", // Địa chỉ của TestERC721
    contract: "contracts/TestERC721.sol:TestERC721",
  });

  await run("verify:verify", {
    address: "0xYourDepositWithdrawContractAddress", // Địa chỉ của DepositWithdraw
    contract: "contracts/DepositWithdraw.sol:DepositWithdraw",
  });
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
