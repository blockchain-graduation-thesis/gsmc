const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("DepositWithdraw Contract", function () {
  let testERC20, testERC721, depositWithdraw;
  let owner, user;
  let amount, tokenId, tokenURI;

  const ERC20_ADDRESS = "0x70518bDa348aBf26000B3818F0Ae2a62ea72e404";
  const ERC721_ADDRESS = "0x0Af887Bdd7651Ea0936977E9E7BE0484eabCdff0";
  const DEPOSITWITHDRAW_ADDRESS = "0x69800760482804933F9Ae0415ca05dF5d3f32C28";

  before(async function () {
    [owner] = await ethers.getSigners();

    const TestERC20 = await ethers.getContractFactory("TestERC20");
    const TestERC721 = await ethers.getContractFactory("TestERC721");
    const DepositWithdraw = await ethers.getContractFactory("DepositWithdraw");

    testERC20 = await TestERC20.attach(ERC20_ADDRESS);
    testERC721 = await TestERC721.attach(ERC721_ADDRESS);
    depositWithdraw = await DepositWithdraw.attach(DEPOSITWITHDRAW_ADDRESS);

    amount = ethers.utils.parseUnits("1000000", 18);

    tokenId = 1;
  });

  it("Logs all info", async function () {
    console.log("Owner address:", owner.address);
    console.log("TestERC20 address:", testERC20.address);
    console.log("TestERC721 address:", testERC721.address);
    console.log("DepositWithdraw address:", depositWithdraw.address);
  });

  // it("Mints ERC20 tokens to owner", async function () {
  //   await testERC20.mint(owner.address, amount);
  //   const balance = await testERC20.balanceOf(owner.address);
  //   console.log("Balance:", balance.toString());
  // });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 9);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 10);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 11);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 12);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 13);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 14);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 15);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 16);
    console.log("Result:", result);
  });
  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 17);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 18);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 19);
    console.log("Result:", result);
  });

  it("Mints ERC721 tokens to owner", async function () {
    const result = await testERC721.mint(owner.address, 20);
    console.log("Result:", result);
  });

  // it("Deposits ERC20 tokens", async function () {
  //   await testERC20.approve(depositWithdraw.address, amount);
  //   const result = await depositWithdraw.depositERC20(
  //     testERC20.address,
  //     amount
  //   );
  //   console.log("Result:", result);
  // });

  // it("Deposits ERC721 tokens", async function () {
  //   await testERC721.approve(depositWithdraw.address, tokenId);
  //   const result = await depositWithdraw.depositERC721(
  //     testERC721.address,
  //     tokenId
  //   );
  //   console.log("Result:", result);
  // });

  // it("Withdraws ERC20 tokens", async function () {
  //   const messageHash = ethers.utils.solidityKeccak256(
  //     ["address", "address", "uint256"],
  //     [owner.address, testERC20.address, amount]
  //   );
  //   const signature = await owner.signMessage(
  //     ethers.utils.arrayify(messageHash)
  //   );

  //   const result = await depositWithdraw.withdrawERC20(
  //     testERC20.address,
  //     amount,
  //     signature
  //   );
  //   console.log("Result:", result);
  // });

  // it("Withdraws ERC721 tokens", async function () {
  //   const messageHash = ethers.utils.solidityKeccak256(
  //     ["address", "address", "uint256"],
  //     [owner.address, testERC721.address, tokenId]
  //   );
  //   const signature = await owner.signMessage(
  //     ethers.utils.arrayify(messageHash)
  //   );

  //   const result = await depositWithdraw.withdrawERC721(
  //     testERC721.address,
  //     tokenId,
  //     signature
  //   );

  //   console.log("Result:", result);
  // });
});
