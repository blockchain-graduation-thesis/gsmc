// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract DepositWithdraw is ERC721Holder, Ownable {
    mapping(address => mapping(address => uint256)) private erc20Deposits;
    mapping(address => mapping(address => uint256[])) private erc721Deposits;

    event ERC20Deposited(
        address indexed user,
        address indexed token,
        uint256 amount
    );
    event ERC20Withdrawn(
        address indexed user,
        address indexed token,
        uint256 amount
    );

    event ERC721Deposited(
        address indexed user,
        address indexed token,
        uint256 tokenId
    );
    event ERC721Withdrawn(
        address indexed user,
        address indexed token,
        uint256 tokenId
    );

    function depositERC20(address token, uint256 amount) external {
        require(amount > 0, "Amount must be greater than zero");
        IERC20(token).transferFrom(msg.sender, address(this), amount);
        erc20Deposits[msg.sender][token] += amount;
        emit ERC20Deposited(msg.sender, token, amount);
    }

    function withdrawERC20(
        address token,
        uint256 amount,
        bytes calldata signature
    ) external {
        require(amount > 0, "Amount must be greater than zero");

        bytes32 messageHash = getMessageHash(msg.sender, token, amount);
        require(verifySignature(messageHash, signature), "Invalid signature");

        IERC20(token).transfer(msg.sender, amount);
        emit ERC20Withdrawn(msg.sender, token, amount);
    }

    function depositERC721(address token, uint256 tokenId) external {
        IERC721(token).safeTransferFrom(msg.sender, address(this), tokenId);
        erc721Deposits[msg.sender][token].push(tokenId);
        emit ERC721Deposited(msg.sender, token, tokenId);
    }

    function withdrawERC721(
        address token,
        uint256 tokenId,
        bytes calldata signature
    ) external {
        bytes32 messageHash = getMessageHash(msg.sender, token, tokenId);
        require(verifySignature(messageHash, signature), "Invalid signature");

        IERC721(token).safeTransferFrom(address(this), msg.sender, tokenId);
        emit ERC721Withdrawn(msg.sender, token, tokenId);
    }

    function getMessageHash(
        address user,
        address token,
        uint256 amount
    ) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(user, token, amount));
    }

    function verifySignature(
        bytes32 messageHash,
        bytes memory signature
    ) public view returns (bool) {
        bytes32 ethSignedMessageHash = getEthSignedMessageHash(messageHash);
        return recoverSigner(ethSignedMessageHash, signature) == owner();
    }

    function getEthSignedMessageHash(
        bytes32 messageHash
    ) public pure returns (bytes32) {
        return
            keccak256(
                abi.encodePacked(
                    "\x19Ethereum Signed Message:\n32",
                    messageHash
                )
            );
    }

    function recoverSigner(
        bytes32 ethSignedMessageHash,
        bytes memory signature
    ) public pure returns (address) {
        (bytes32 r, bytes32 s, uint8 v) = splitSignature(signature);
        return ecrecover(ethSignedMessageHash, v, r, s);
    }

    function splitSignature(
        bytes memory signature
    ) public pure returns (bytes32 r, bytes32 s, uint8 v) {
        require(signature.length == 65, "Invalid signature length");

        assembly {
            r := mload(add(signature, 32))
            s := mload(add(signature, 64))
            v := byte(0, mload(add(signature, 96)))
        }
    }
}
