require("@nomiclabs/hardhat-ethers");
require("hardhat-deploy");
require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");

module.exports = {
  solidity: "0.8.18",
  networks: {
    sepolia: {
      url: "https://eth-sepolia.api.onfinality.io/public",
      accounts: [
        "5f5d27f8df70cd6f8a496d84b434567b74fc81b707040797d1c60222b74a8a71",
      ],
    },
  },
  etherscan: {
    apiKey: "EC7KP6CMPVYJ3CJUIZEWVZWAX8JB2YBUZU",
  },
};
